from unittest import TestCase

from core.emulator.coreemu import CoreEmu
from core.emulator.enumerations import EventTypes


class TestCaseCore(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.coreemu = CoreEmu()

    @classmethod
    def tearDownClass(cls):
        """We cannot perform manual cleanup of CoreEmu since it ALWAYS performs
        a cleanup on terminating signals."""
        pass

    def setUp(self):
        """Override but call super method and instantiate your network. Finally start the
        session."""
        self.session = self.coreemu.create_session()
        self.session.set_state(EventTypes.CONFIGURATION_STATE)

    def tearDown(self):
        self.coreemu.delete_session(self.session.id)
