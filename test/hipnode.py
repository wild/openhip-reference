import copy
import queue
import re
import time
import uuid
from enum import Enum
from queue import Queue
from subprocess import PIPE, Popen
from threading import Thread
from xml.etree import ElementTree

from core.nodes.base import CoreNode

from utils import XML


class State(Enum):
    IDLE = 0
    READY = 1
    RUNNING = 2


class HIPNode(CoreNode):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.state = State.IDLE

    def shutdown(self):
        self.stop()
        super().shutdown()

    def _state_check(self, *states):
        if self.state not in states:
            raise RuntimeError(f"Current state {self.state} is not one of {states}")

    def change_ip(self, ip: str):
        self.cmd(f"ip address del {self.ip}/24 dev eth0")
        self.find(r".*?HIP association 0 moved from R2_SENT=>ESTABLISHED.*?")
        self.ip = ip
        self.cmd(f"ip address add {self.ip}/24 dev eth0")
        self.find(rf".*?Address added.*?{self.ip}.*?")

    def hitgen(self, public_path: str, ip: str = None, args: str = "", conf: bool = False):
        """Generate host identities and append to the publicly known host identities"""
        self._state_check(State.IDLE, State.READY)

        if not ip:
            eth0 = self.get_ifaces()[0]
            ip = str(eth0.get_ip4().ip)
        self.ip = ip

        self.cmd("mkdir -p /usr/local/etc/hip")
        self.cmd("mount --bind . /usr/local/etc/hip")
        self.cmd("touch known_host_identities.xml")
        self.cmd(f"mount --bind {public_path} known_host_identities.xml")
        self.cmd("hitgen -conf")
        self.cmd(f"hitgen -noinput {args}")
        self.cmd("hitgen -publish -file public.xml")

        if conf:
            path = f"{self.nodedir}/hip.conf"
            tree = ElementTree.parse(path)
            root = tree.getroot()
            suites = root.find("available_hit_suites")[0]
            element = ElementTree.SubElement(suites, "suite")
            element.text = "5"
            suites.insert(0, element)
            tree.write(path, encoding="utf-8", xml_declaration=True)

        tree = ElementTree.parse(f"{self.nodedir}/public.xml")
        root = tree.getroot()
        host_identity = copy.deepcopy(root[0])
        host_identity.append(ElementTree.fromstring(f"<addr>{self.ip}</addr>"))
        XML.append_root(public_path, host_identity)

        self.LSI = host_identity.find("LSI").text
        self.state = State.READY

    def command(self, cmd, *args, **kwargs):
        return Popen(f"vcmd -c {self.ctrlchnlname} -- {cmd}".split(" "), *args, **kwargs)

    def dump(self):
        for line in self.clear_queue():
            print(line)

    def start(self):
        self._state_check(State.READY)

        self.hip = self.command("hip -v", stdout=PIPE, universal_newlines=True)
        self.queue = Queue()
        self.thread = Thread(target=self._enqueue)
        self.thread.daemon = True
        self.thread.start()
        self.state = State.RUNNING
        self.find(r".*?HIP threads initialization completed.*?")

    def _enqueue(self):
        while line := self.hip.stdout.readline():
            self.queue.put(line[:-1])

    def clear_queue(self):
        lines = []
        while True:
            try:
                lines.append(self.queue.get_nowait())
            except queue.Empty:
                break
        return lines

    def find(self, pattern: str, timeout=15):
        start = time.time()
        while time.time() - start < timeout:
            try:
                line = self.queue.get_nowait()
                if match := re.match(pattern, line):
                    return match
            except queue.Empty:
                pass

        raise TimeoutError(
            f"Failed to find pattern {pattern} within {timeout} seconds on host {self.name}"
        )

    def stop(self):
        if self.state == State.RUNNING:
            self.hip.kill()
            self.hip.wait()
            self.state = State.READY
