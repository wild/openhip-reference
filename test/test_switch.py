import unittest
from subprocess import PIPE

from core.emulator.data import IpPrefixes
from core.nodes.network import SwitchNode

from hipnode import HIPNode
from testcasecore import TestCaseCore
from utils import XML


class Test(TestCaseCore):
    """Tests between two computers and a switch."""

    N1_HITGEN_KWARGS = {}
    N2_HITGEN_KWARGS = {}

    def setUp(self):
        super().setUp()
        ip_prefixes = IpPrefixes(ip4_prefix="10.0.0.0/24")

        switch: SwitchNode = self.session.add_node(SwitchNode)
        self.n1: HIPNode = self.session.add_node(HIPNode)
        self.n2: HIPNode = self.session.add_node(HIPNode)
        iface1 = ip_prefixes.create_iface(self.n1)
        self.session.add_link(self.n1.id, switch.id, iface1)
        iface2 = ip_prefixes.create_iface(self.n2)
        self.session.add_link(self.n2.id, switch.id, iface2)
        self.session.instantiate()

        public = f"{self.session.session_dir}/known_host_identities.xml"
        XML.create_file(public, "known_host_identities")
        self.n1.hitgen(public, **self.N1_HITGEN_KWARGS)
        self.n2.hitgen(public, **self.N2_HITGEN_KWARGS)
        self.n1.start()
        self.n2.start()

    def test_basic_connectivity(self):
        """Test that two computers connected by a switch can communicate via HIP."""
        self.assertEqual(self.n1.command(f"ping -c 1 -W 5 {self.n2.LSI}", stdout=PIPE).wait(), 0)
        self.assertIsNotNone(self.n1.find(r".*?HIP exchange complete.*?"))
        self.assertIsNotNone(self.n2.find(r".*?HIP exchange complete.*?"))

    def test_address_mobility(self):
        """Test that two computers continue to communicate via HIP during address change."""
        self.n1.command(f"ping -c 1 -W 5 {self.n2.LSI}", stdout=PIPE).wait()
        # make sure that base exchange has already been made
        self.n1.clear_queue()

        ping = self.n1.command(f"ping -c 5 -W 2 {self.n2.LSI}", stdout=PIPE)
        self.n2.change_ip("10.0.0.30")
        self.assertEqual(ping.wait(), 0)
        self.assertIsNotNone(self.n1.find(r".*?Update completed \(readdress\).*?"))
        self.assertIsNotNone(self.n2.find(r".*?Update completed \(rekey\).*?"))


class TestAllHHIT(Test):
    """Tests between two computers and a switch where both are using HHITs."""

    N1_HITGEN_KWARGS = {"args": "-hhit _n1_"}
    N2_HITGEN_KWARGS = {"args": "-hhit _n2_"}


class TestOneHHIT(Test):
    """Tests between two computers and a switch where one is using HHITs."""

    N1_HITGEN_KWARGS = {}
    N2_HITGEN_KWARGS = {"args": "-hhit _n2_"}


class TestEdDSA25519(Test):
    """Tests between two computers and a switch where both are using EdDSA Curve25519."""

    N1_HITGEN_KWARGS = {"args": "-type EdDSA -suite 5 -curve 1", "conf": True}
    N2_HITGEN_KWARGS = {"args": "-type EdDSA -suite 5 -curve 1", "conf": True}


class TestEdDSA448(Test):
    """Tests between two computers and a switch where both are using EdDSA Curve448."""

    N1_HITGEN_KWARGS = {"args": "-type EdDSA -suite 5 -curve 3", "conf": True}
    N2_HITGEN_KWARGS = {"args": "-type EdDSA -suite 5 -curve 3", "conf": True}


if __name__ == "__main__":
    unittest.main(verbosity=2, warnings="ignore")
