from xml.etree import ElementTree
from xml.etree.ElementTree import Element


class XML:
    @staticmethod
    def create_file(path: str, root_name: str):
        root = ElementTree.Element(root_name)
        tree = ElementTree.ElementTree(root)
        tree.write(path, encoding="utf-8", xml_declaration=True)

    @staticmethod
    def append_root(path: str, element: Element):
        tree = ElementTree.parse(path)
        root = tree.getroot()
        root.append(element)
        tree.write(path, encoding="utf-8", xml_declaration=True)
